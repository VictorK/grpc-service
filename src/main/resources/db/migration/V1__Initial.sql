CREATE TABLE IF NOT EXISTS public.account(
  id integer NOT NULL,
  balance bigint NOT NULL,
  CONSTRAINT account_pk PRIMARY KEY(id)
);
