package com.vic.service;

import com.vic.dao.AccountDao;
import com.vic.model.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class AccountServiceImpl implements AccountService {

    private Map<Integer, AtomicLong> balance = new ConcurrentHashMap<>();
    private final AccountDao accountDao;

    @Autowired
    public AccountServiceImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @PostConstruct
    public void init() {
        List<Account> list = accountDao.getList();
        list.forEach(account -> balance.putIfAbsent(account.getId(), new AtomicLong(account.getBalance())));
    }

    @Override
    public Long getAmount(Integer id) {
        return balance.getOrDefault(id, new AtomicLong(0L)).get();
    }

    @Override
    public void addAmount(Integer id, Long value) {
        balance.putIfAbsent(id, new AtomicLong(0L));
        final AtomicLong atomicLong = balance.get(id);
        atomicLong.addAndGet(value);
        accountDao.addAmount(id, value);
    }
}
