package com.vic.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Service
@Slf4j
public class StatisticService {

    private AtomicLong counterGetAmount = new AtomicLong(0L);
    private AtomicLong counterAddAmount = new AtomicLong(0L);
    private Map<Integer, AtomicLong> userStatisticGetAmount = new ConcurrentHashMap<>();
    private Map<Integer, AtomicLong> userStatisticAddAmount = new ConcurrentHashMap<>();

    public void incrementCounterGetAmount() {
        counterGetAmount.incrementAndGet();
    }

    public void incrementCounterAddAmount() {
        counterAddAmount.incrementAndGet();
    }

    public void incrementUserStatisticGetAmount(Integer id) {
        userStatisticGetAmount.putIfAbsent(id, new AtomicLong(0L));
        final AtomicLong atomicLong = userStatisticGetAmount.get(id);
        atomicLong.incrementAndGet();
        incrementCounterGetAmount();
    }

    public void incrementUserStatisticAddAmount(Integer id) {
        userStatisticAddAmount.putIfAbsent(id, new AtomicLong(0L));
        final AtomicLong atomicLong = userStatisticAddAmount.get(id);
        atomicLong.incrementAndGet();
        incrementCounterAddAmount();
    }

    public void clearUserStatistic() {
        userStatisticGetAmount.clear();
        userStatisticAddAmount.clear();
    }

    @Scheduled(cron = "*/1 * * * * *")
    public void printStatistic() {
        final Long valueGetAmount = counterGetAmount.getAndSet(0L);
        final Long valueAddAmount = counterAddAmount.getAndSet(0L);
        final StringBuilder builder = new StringBuilder();
        builder.append("getAmount:\n");
        userStatisticGetAmount.entrySet().forEach(entry -> {
            builder.append("User with id '").append(entry.getKey()).append("': ")
                    .append(entry.getValue().get()).append("\n");
        });
        builder.append("Request ").append(valueGetAmount).append(" per second\n").append("addAmount:\n");
        userStatisticAddAmount.entrySet().forEach(entry -> {
            builder.append("User with id '").append(entry.getKey()).append("': ")
                    .append(entry.getValue().get()).append("\n");
        });
        builder.append("Request ").append(valueAddAmount).append(" per second\n");
        if (valueGetAmount.compareTo(0L) > 0 || valueAddAmount.compareTo(0L) > 0) {
            log.info(builder.toString());
        }
    }
}
