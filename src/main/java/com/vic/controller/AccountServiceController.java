package com.vic.controller;

import com.vic.dto.BalanceProtos;
import com.vic.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/balance")
public class AccountServiceController {

    private final AccountService service;

    @Autowired
    public AccountServiceController(AccountService service) {
        this.service = service;
    }

    @GetMapping(value = "/{id}")
    public BalanceProtos.Balance getAmount(@PathVariable("id") Integer id) {
        return BalanceProtos.Balance.newBuilder()
                .setId(id)
                .setValue(service.getAmount(id))
                .build();
    }

    @PutMapping
    public void addAmount(@RequestBody BalanceProtos.Balance balance) {
        service.addAmount(balance.getId(), balance.getValue());
    }
}
