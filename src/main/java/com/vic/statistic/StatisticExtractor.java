package com.vic.statistic;

import com.vic.service.StatisticService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class StatisticExtractor {

    private final StatisticService service;

    @Autowired
    public StatisticExtractor(StatisticService service) {
        this.service = service;
    }

    @Pointcut("execution(* com.vic.service.AccountServiceImpl.getAmount(..))")
    public void selectGetAmount() {
    }

    @Pointcut("execution(* com.vic.service.AccountServiceImpl.addAmount(..))")
    public void selectAddAmount() {
    }

    @After("selectGetAmount()")
    public void afterGetAmount(JoinPoint joinPoint) {
        service.incrementUserStatisticGetAmount((Integer) joinPoint.getArgs()[0]);
    }

    @After("selectAddAmount()")
    public void afterAddAmount(JoinPoint joinPoint) {
        service.incrementUserStatisticAddAmount((Integer) joinPoint.getArgs()[0]);
    }
}
