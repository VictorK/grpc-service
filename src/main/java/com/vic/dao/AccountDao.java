package com.vic.dao;

import com.vic.model.entity.Account;
import com.vic.model.mapper.AccountRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;

@Component
public class AccountDao {

    static final String FIND_ALL_ACCOUNT = "SELECT * FROM public.account";
    static final String UPSERT_ACCOUNT = "INSERT INTO public.account AS a (id, balance) VALUES(?, ?) " +
            "ON CONFLICT (id) DO UPDATE SET balance = a.balance + EXCLUDED.balance";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AccountDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Account> getList() {
        return jdbcTemplate.query(FIND_ALL_ACCOUNT, new AccountRowMapper());
    }

    public void addAmount(Integer id, Long value) {
        Assert.notNull(id, "Id must not be null");
        Assert.notNull(value, "Value must not be null");
        jdbcTemplate.update(UPSERT_ACCOUNT, id, value);
    }
}
